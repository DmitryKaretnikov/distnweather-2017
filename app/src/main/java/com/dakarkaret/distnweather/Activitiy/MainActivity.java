package com.dakarkaret.distnweather.Activitiy;

import android.Manifest;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;

import com.dakarkaret.distnweather.CardsScreen.CardsFragmentImp;
import com.dakarkaret.distnweather.EditScreen.EditFragmentImp;
import com.dakarkaret.distnweather.Helper.LocationHelper;
import com.dakarkaret.distnweather.Data.EventMessage;
import com.dakarkaret.distnweather.R;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import permissions.dispatcher.NeedsPermission;
import permissions.dispatcher.RuntimePermissions;

@RuntimePermissions
public class MainActivity extends AppCompatActivity {
    private FragmentManager mFragmentManager;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_single);

        MainActivityPermissionsDispatcher.checkPermWithCheck(this);
        mFragmentManager = getSupportFragmentManager();
        if (savedInstanceState == null) {
            setCardsFragment();
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        LocationHelper.getInstance().on();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onPause() {
        LocationHelper.getInstance().off();
        EventBus.getDefault().unregister(this);
        super.onPause();
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void setEditFragment(EventMessage eventMessage) {
        int id = eventMessage.getId();
        mFragmentManager.beginTransaction()
                .setCustomAnimations(R.anim.anim_in, R.anim.anim_out)
                .replace(R.id.activity_single_fragment_container, EditFragmentImp.newInstance(id))
                .addToBackStack(null)
                .commit();
    }

    private void setCardsFragment() {
        mFragmentManager.beginTransaction()
                .replace(R.id.activity_single_fragment_container, CardsFragmentImp.newInstance())
                .commit();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        MainActivityPermissionsDispatcher.onRequestPermissionsResult(this, requestCode, grantResults);
    }

    @NeedsPermission({Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.ACCESS_FINE_LOCATION})
    void checkPerm() {
    }
}
