package com.dakarkaret.distnweather.Activitiy;

import android.animation.AnimatorInflater;
import android.animation.AnimatorSet;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.widget.ImageView;

import com.dakarkaret.distnweather.Helper.ContextSingleton;
import com.dakarkaret.distnweather.R;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

public class SplashActivity extends AppCompatActivity {
    @BindView(R.id.activity_splash_image)
    ImageView mImageView;
    private Unbinder mUnbinder;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        ContextSingleton.setContextSingleton(this);
        mUnbinder = ButterKnife.bind(this);
    }

    @Override
    protected void onResume() {
        super.onResume();
        AnimatorSet set = new AnimatorSet();
        set.playTogether(
                AnimatorInflater.loadAnimator(ContextSingleton.getContext(), R.animator.animator_rotate).setDuration(600),
                AnimatorInflater.loadAnimator(ContextSingleton.getContext(), R.animator.animator_scale),
                AnimatorInflater.loadAnimator(ContextSingleton.getContext(), R.animator.animator_alpha)
        );
        set.setTarget(mImageView);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                startActivity(new Intent(SplashActivity.this, MainActivity.class));
                overridePendingTransition(R.anim.anim_in, R.anim. anim_out);
                finish();
            }
        }, 600);
        set.start();
    }

    @Override
    public void onDestroy() {
        mUnbinder.unbind();
        super.onDestroy();
    }
}
