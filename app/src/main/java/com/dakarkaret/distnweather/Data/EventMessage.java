package com.dakarkaret.distnweather.Data;

public class EventMessage {
    private final int id;

    public EventMessage(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }
}
