package com.dakarkaret.distnweather.Data;

import android.content.ContentValues;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.dakarkaret.distnweather.Helper.ContextSingleton;

public class CityDbHelper extends SQLiteOpenHelper {
    private static final String DATABASE_NAME = "distnweather.db";
    private static final int DATABASE_VERSION = 1;

    public CityDbHelper() {
        super(ContextSingleton.getContext(), DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(CityTable.createTable());
        insertCity(db);
    }

    private void insertCity(SQLiteDatabase db) {
        ContentValues valuesK = new ContentValues();
        valuesK.put(CityTable.COLUMN_NAME, "Kiev");
        valuesK.put(CityTable.COLUMN_LATITUDE, 50.4501);
        valuesK.put(CityTable.COLUMN_LONGITUDE, 30.5234);
        valuesK.put(CityTable.IMAGE_URL, "https://upload.wikimedia.org/wikipedia/commons/thumb/3/35/Flag_of_Kyiv_Kurovskyi.svg/900px-Flag_of_Kyiv_Kurovskyi.svg.png");

        db.insert(CityTable.TABLE_NAME, null, valuesK);

        ContentValues valuesO = new ContentValues();
        valuesO.put(CityTable.COLUMN_NAME, "Odessa");
        valuesO.put(CityTable.COLUMN_LATITUDE, 46.4846);
        valuesO.put(CityTable.COLUMN_LONGITUDE, 30.7326);
        valuesO.put(CityTable.IMAGE_URL, "https://upload.wikimedia.org/wikipedia/commons/thumb/5/5d/Flag_of_Odessa.svg/1024px-Flag_of_Odessa.svg.png ");

        db.insert(CityTable.TABLE_NAME, null, valuesO);

        ContentValues valuesL = new ContentValues();
        valuesL.put(CityTable.COLUMN_NAME, "Lviv");
        valuesL.put(CityTable.COLUMN_LATITUDE, 49.8397);
        valuesL.put(CityTable.COLUMN_LONGITUDE, 24.0297);
        valuesL.put(CityTable.IMAGE_URL, "https://upload.wikimedia.org/wikipedia/commons/thumb/5/54/Flag_of_Lviv.svg/500px-Flag_of_Lviv.svg.png");

        db.insert(CityTable.TABLE_NAME, null, valuesL);

        ContentValues valuesKh = new ContentValues();
        valuesKh.put(CityTable.COLUMN_NAME, "Kharkiv");
        valuesKh.put(CityTable.COLUMN_LATITUDE, 49.9935);
        valuesKh.put(CityTable.COLUMN_LONGITUDE, 36.2304);
        valuesKh.put(CityTable.IMAGE_URL, "https://upload.wikimedia.org/wikipedia/commons/thumb/c/cf/Kharkiv-town-flag.svg/800px-Kharkiv-town-flag.svg.png");

        db.insert(CityTable.TABLE_NAME, null, valuesKh);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
    }
}
