package com.dakarkaret.distnweather.Data;

import android.provider.BaseColumns;

public final class CityTable implements BaseColumns {
    public final static String TABLE_NAME = "Cities";
    public final static String _ID = BaseColumns._ID;
    public final static int NEW_CITY = -1;
    final static String COLUMN_NAME = "name";
    final static String COLUMN_LONGITUDE = "longitude";
    final static String COLUMN_LATITUDE = "latitude";
    final static String COLUMN_WEATHER = "weather";
    final static String IMAGE_URL = "imageURL";

    static String createTable() {
        return "CREATE TABLE " + TABLE_NAME + " ("
                + _ID + " INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, "
                + COLUMN_NAME + " TEXT NOT NULL, "
                + COLUMN_LATITUDE + " REAL NOT NULL, "
                + COLUMN_LONGITUDE + " REAL NOT NULL, "
                + COLUMN_WEATHER + " TEXT NOT NULL DEFAULT 0, "
                + IMAGE_URL + " TEXT NOT NULL);";
    }
}