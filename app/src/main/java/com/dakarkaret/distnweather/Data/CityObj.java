package com.dakarkaret.distnweather.Data;

import com.pushtorefresh.storio.sqlite.annotations.StorIOSQLiteColumn;
import com.pushtorefresh.storio.sqlite.annotations.StorIOSQLiteType;

@StorIOSQLiteType(table = CityTable.TABLE_NAME)
public class CityObj {
    @StorIOSQLiteColumn(name = CityTable.COLUMN_LATITUDE)
    float Lat = 0;
    @StorIOSQLiteColumn(name = CityTable.COLUMN_LONGITUDE)
    float Lng = 0;
    @StorIOSQLiteColumn(name = CityTable.COLUMN_NAME)
    String Name = "nullName";
    @StorIOSQLiteColumn(name = CityTable.IMAGE_URL)
    String imageUrl = "nullUrl";
    @StorIOSQLiteColumn(name = CityTable.COLUMN_WEATHER)
    String Weather = "0";
    @StorIOSQLiteColumn(name = CityTable._ID, key = true)
    int id;

    public CityObj() {
    }

    public float getLat() {
        return Lat;
    }

    public void setLat(float lat) {
        Lat = lat;
    }

    public float getLng() {
        return Lng;
    }

    public void setLng(float lng) {
        Lng = lng;
    }

    public String getWeather() {
        return Weather;
    }

    public void setWeather(String weather) {
        Weather = weather;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
