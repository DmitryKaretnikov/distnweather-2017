package com.dakarkaret.distnweather.CardsScreen;

import com.dakarkaret.distnweather.Data.CityDbHelper;
import com.dakarkaret.distnweather.Data.CityObj;
import com.dakarkaret.distnweather.Data.CityObjSQLiteTypeMapping;
import com.dakarkaret.distnweather.Data.CityTable;
import com.dakarkaret.distnweather.Data.WeatherObj;
import com.dakarkaret.distnweather.Helper.ContextSingleton;
import com.dakarkaret.distnweather.Helper.LocationHelper;
import com.dakarkaret.distnweather.Helper.OpenWeatherClientInterface;
import com.dakarkaret.distnweather.Helper.PermHelper;
import com.dakarkaret.distnweather.Helper.PrefsHelper;
import com.dakarkaret.distnweather.Helper.ServiceGenerator;
import com.dakarkaret.distnweather.R;
import com.pushtorefresh.storio.sqlite.impl.DefaultStorIOSQLite;
import com.pushtorefresh.storio.sqlite.queries.Query;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

class CardsPresenterImp implements CardsPresenter {
    private DefaultStorIOSQLite mDefaultStorIOSQLite;
    private CardsFragment mCardsFragment;
    private int callsCounter = 0;

    private CardsPresenterImp(CardsFragment cardsFragment) {
        mCardsFragment = cardsFragment;

        CityDbHelper sqLiteOpenHelper = new CityDbHelper();

        mDefaultStorIOSQLite = DefaultStorIOSQLite.builder()
                .sqliteOpenHelper(sqLiteOpenHelper)
                .addTypeMapping(CityObj.class, new CityObjSQLiteTypeMapping())
                .build();
    }

    static CardsPresenterImp newInstance(CardsFragment cardsFragment) {
        return new CardsPresenterImp(cardsFragment);
    }

    public void deleteCity(int id, String cityName) {
        CityObj cityObj = new CityObj();
        cityObj.setId(id);
        mDefaultStorIOSQLite
                .delete()
                .object(cityObj)
                .prepare()
                .executeAsBlocking();
        mCardsFragment.showSnackbar(cityName + " was deleted!");
    }

    public void updateAll() {
        updateLocation();
        loadCities();
        updateWeather();
    }

    public void updateData() {
        updateLocation();
        loadCities();
    }

    private void updateLocation() {
        if (PermHelper.checkLocPerm())
            PrefsHelper.getInstance().saveCoord(LocationHelper.getInstance().getLongitude(), LocationHelper.getInstance().getLatitude());
        else
            PrefsHelper.getInstance().saveCoord(PrefsHelper.NO_PERM, PrefsHelper.NO_PERM);
    }

    private void loadCities() {
        List<CityObj> list = mDefaultStorIOSQLite.get()
                .listOfObjects(CityObj.class)
                .withQuery(
                        Query.builder()
                                .table(CityTable.TABLE_NAME)
                                .build())
                .prepare()
                .executeAsBlocking();
        mCardsFragment.setAdapterList(new ArrayList<>(list));
    }

    private void updateWeather() {
        OpenWeatherClientInterface client = ServiceGenerator.createService(OpenWeatherClientInterface.class);

        if (mCardsFragment.getList().size() < 1) {
            callsCounterHelper(false);
        }

        for (final CityObj city : mCardsFragment.getList()) {
            Call<WeatherObj> call = client.getWeatherObj(String.valueOf(city.getLat()), String.valueOf(city.getLng()),
                    "metric", ContextSingleton.getContext().getResources().getString(R.string.open_weather_key));
            callsCounterHelper(true);
            call.enqueue(new Callback<WeatherObj>() {
                @Override
                public void onResponse(Call<WeatherObj> call, Response<WeatherObj> response) {
                    if (response.body() != null) {
                        String temp = new DecimalFormat("##.#").format(response.body().getTemp());
                        city.setWeather(temp);
                        mDefaultStorIOSQLite
                                .put()
                                .object(city)
                                .prepare()
                                .executeAsBlocking();
                    }
                    callsCounterHelper(false);
                }

                @Override
                public void onFailure(Call<WeatherObj> call, Throwable t) {
                    callsCounterHelper(false);
                }
            });
        }
    }

    private synchronized void callsCounterHelper(boolean plus) {
        if (plus) {
            callsCounter++;
        } else {
            callsCounter--;
            if (callsCounter <= 0) {
                mCardsFragment.stopRefreshing();
            }
        }
    }
}
