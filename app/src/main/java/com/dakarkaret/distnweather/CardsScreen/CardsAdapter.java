package com.dakarkaret.distnweather.CardsScreen;

import android.support.v4.content.ContextCompat;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.dakarkaret.distnweather.Helper.ContextSingleton;
import com.dakarkaret.distnweather.Helper.PrefsHelper;
import com.dakarkaret.distnweather.Data.CityObj;
import com.dakarkaret.distnweather.R;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.OnLongClick;
import butterknife.OnTouch;

class CardsAdapter extends RecyclerView.Adapter<CardsAdapter.CityHolder> {
    private CardsFragment mCardsFragment;
    private ArrayList<CityObj> mCityList;
    private ArrayList<CityObj> mFilteredCityList;
    private String mFilterString = "";

    private CardsAdapter(CardsFragment cardsFragment) {
        mCardsFragment = cardsFragment;
        mCityList = new ArrayList<>();
        mFilteredCityList = new ArrayList<>();
    }

    static CardsAdapter newInstance(CardsFragment cardsFragment) {
        return new CardsAdapter(cardsFragment);
    }

    @Override
    public CityHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.card_city, parent, false);
        return new CityHolder(view, mCardsFragment);
    }

    @Override
    public void onBindViewHolder(CityHolder holder, int position) {
        holder.city = mFilteredCityList.get(position);

        holder.mNameText.setText(holder.city.getName());
        holder.mLocationText.setText(PrefsHelper.getInstance().getDistFrom(holder.city.getLat(), holder.city.getLng()));
        holder.mWeatherText.setText(holder.city.getWeather() + "\u2103");
        loadImage(holder);
    }

    private void loadImage(final CityHolder holder) {
        Picasso.with(ContextSingleton.getContext())
                .load(holder.city.getImageUrl())
                .placeholder(R.drawable.load_img)
                .error(R.drawable.error_img)
                .into(holder.mImageView, new Callback() {
                    @Override
                    public void onSuccess() {
                    }

                    @Override
                    public void onError() {
                    }
                });
    }

    void deleteCity(int posId) {
        mCityList.remove(mFilteredCityList.get(posId));
        mFilteredCityList.remove(posId);
        notifyItemRemoved(posId);
    }

    @Override
    public int getItemCount() {
        return mFilteredCityList.size();
    }

    ArrayList<CityObj> getList() {
        return mCityList;
    }

    void setList(ArrayList<CityObj> cityList) {
        mCityList = cityList;
        filter();
    }

    void setFilterString(String s) {
        mFilterString = s.toLowerCase();
        filter();
    }

    private void filter() {
        mFilteredCityList.clear();
        if (mFilterString.isEmpty()) {
            mFilteredCityList.addAll(mCityList);
        } else {
            for (CityObj cityObj : mCityList) {
                if (cityObj.getName().toLowerCase().contains(mFilterString)) {
                    mFilteredCityList.add(cityObj);
                }
            }
        }
        notifyDataSetChanged();
    }

    static class CityHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.card_list)
        CardView mCardView;
        @BindView(R.id.card_city)
        TextView mNameText;
        @BindView(R.id.card_location)
        TextView mLocationText;
        @BindView(R.id.card_weather)
        TextView mWeatherText;
        @BindView(R.id.card_image)
        ImageView mImageView;
        CityObj city;
        private CardsFragment mVHCardsFragment;

        CityHolder(View itemView, CardsFragment cardsFragment) {
            super(itemView);
            mVHCardsFragment = cardsFragment;
            ButterKnife.bind(this, itemView);
        }

        @OnClick(R.id.card_list)
        void startEdit() {
            mVHCardsFragment.startEdit(city.getId());
        }

        @OnLongClick(R.id.card_list)
        boolean callDelDialog() {
            mVHCardsFragment.callDelDialog(city.getName(), city.getId(), getLayoutPosition());
            return true;
        }

        @OnTouch(R.id.card_list)
        boolean changeCardBackground(MotionEvent event) {
            switch (event.getAction()) {
                case MotionEvent.ACTION_DOWN:
                    mCardView.setCardBackgroundColor(ContextCompat.getColor(ContextSingleton.getContext(), R.color.color_card_taped));
                    break;
                case MotionEvent.ACTION_UP:
                case MotionEvent.ACTION_CANCEL:
                    mCardView.setCardBackgroundColor(ContextCompat.getColor(ContextSingleton.getContext(), R.color.color_card));
                    break;
            }
            return false;
        }

        int getCityId() {
            return city.getId();
        }

        String getCityName() {
            return city.getName();
        }
    }
}
