package com.dakarkaret.distnweather.CardsScreen;

import com.dakarkaret.distnweather.Data.CityObj;

import java.util.ArrayList;

interface CardsFragment {
    void callDelDialog(String name, int id, int pos);

    void showSnackbar(String s);

    void startEdit(int id);

    void stopRefreshing();

    ArrayList<CityObj> getList();

    void setAdapterList(ArrayList<CityObj> filteredCityList);
}
