package com.dakarkaret.distnweather.CardsScreen;

import android.content.DialogInterface;
import android.content.res.Configuration;
import android.graphics.Color;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;

import com.dakarkaret.distnweather.Data.CityObj;
import com.dakarkaret.distnweather.Data.CityTable;
import com.dakarkaret.distnweather.Data.EventMessage;
import com.dakarkaret.distnweather.R;

import org.greenrobot.eventbus.EventBus;

import java.util.ArrayList;

public class CardsFragmentImp extends Fragment implements CardsFragment {
    private SwipeRefreshLayout mSwipeRefreshLayout;
    private CardsPresenter mCardsPresenter;
    private CardsAdapter mCardsAdapter;

    public static CardsFragmentImp newInstance() {
        return new CardsFragmentImp();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
        setHasOptionsMenu(true);

        mCardsAdapter = CardsAdapter.newInstance(this);
        mCardsPresenter = CardsPresenterImp.newInstance(this);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        final View v = inflater.inflate(R.layout.fragment_cards, container, false);

        if (container != null) {
            container.clearDisappearingChildren();
        }

        initRecycler(v);
        initSwipeRefresh(v);
        mSwipeRefreshLayout.setRefreshing(true);
        mCardsPresenter.updateAll();
        return v;
    }

    private void initSwipeRefresh(View v) {
        mSwipeRefreshLayout = (SwipeRefreshLayout) v.findViewById(R.id.cards_swipe);
        mSwipeRefreshLayout.setColorSchemeColors(Color.RED, Color.GREEN, Color.BLUE, Color.CYAN);
        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                if (mSwipeRefreshLayout.isRefreshing()) {
                    mSwipeRefreshLayout.setRefreshing(true);
                    mCardsPresenter.updateAll();
                }
            }
        });
    }

    private void initRecycler(View v) {
        RecyclerView recyclerView = (RecyclerView) v.findViewById(R.id.list_recycler);
        if (getResources().getConfiguration().orientation == Configuration.ORIENTATION_PORTRAIT)
            recyclerView.setLayoutManager(new StaggeredGridLayoutManager(2, 1));
        else
            recyclerView.setLayoutManager(new StaggeredGridLayoutManager(3, 1));

        ItemTouchHelper itemTouchHelper = new ItemTouchHelper(new ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.LEFT | ItemTouchHelper.RIGHT) {
            @Override
            public boolean onMove(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, RecyclerView.ViewHolder target) {
                return false;
            }

            @Override
            public void onSwiped(RecyclerView.ViewHolder viewHolder, int direction) {
                CardsAdapter.CityHolder holder = (CardsAdapter.CityHolder) viewHolder;
                mCardsPresenter.deleteCity(holder.getCityId(), holder.getCityName());
                mCardsAdapter.deleteCity(viewHolder.getAdapterPosition());
            }
        });
        itemTouchHelper.attachToRecyclerView(recyclerView);

        recyclerView.setAdapter(mCardsAdapter);
    }

    @Override
    public void onCreateOptionsMenu(final Menu menu, MenuInflater menuInflater) {
        super.onCreateOptionsMenu(menu, menuInflater);
        menuInflater.inflate(R.menu.menu_list, menu);

        MenuItem searchItem = menu.findItem(R.id.menu_list_item_search);
        SearchView mSearchView = (SearchView) searchItem.getActionView();
        mSearchView.setImeOptions(EditorInfo.IME_FLAG_NO_EXTRACT_UI);
        mSearchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String s) {
                mCardsAdapter.setFilterString(s);
                return true;
            }

            @Override
            public boolean onQueryTextChange(String s) {
                mCardsAdapter.setFilterString(s);
                return true;
            }
        });
        mSearchView.setOnSearchClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                menu.findItem(R.id.menu_list_item_add).setVisible(false);
            }
        });
        mSearchView.setOnCloseListener(new SearchView.OnCloseListener() {
            @Override
            public boolean onClose() {
                menu.findItem(R.id.menu_list_item_add).setVisible(true);
                mCardsAdapter.setFilterString("");
                return false;
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_list_item_add:
                startEdit(CityTable.NEW_CITY);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        getActivity().setTitle("DistNWeather");
    }

    @Override
    public void onResume() {
        super.onResume();
        mCardsPresenter.updateData();
    }

    @Override
    public void onPause() {
        mCardsAdapter.setFilterString("");
        super.onPause();
    }

    public void callDelDialog(final String cityName, final int cityId, final int pos) {
        new AlertDialog.Builder(getActivity())
                .setTitle(cityName)
                .setMessage("Are you wanna to delete " + cityName + "?")
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        mCardsPresenter.deleteCity(cityId, cityName);
                        mCardsAdapter.deleteCity(pos);
                    }
                })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                    }
                })
                .setCancelable(true)
                .show();
    }

    public void showSnackbar(String s) {
        Snackbar.make(this.getView(), s, Snackbar.LENGTH_SHORT).show();
    }

    public void startEdit(int id) {
        EventBus.getDefault().post(new EventMessage(id));
    }

    public void stopRefreshing() {
        mSwipeRefreshLayout.setRefreshing(false);
        mCardsAdapter.notifyDataSetChanged();
    }

    public ArrayList<CityObj> getList() {
        return mCardsAdapter.getList();
    }

    public void setAdapterList(ArrayList<CityObj> filteredCityList) {
        mCardsAdapter.setList(filteredCityList);
    }
}