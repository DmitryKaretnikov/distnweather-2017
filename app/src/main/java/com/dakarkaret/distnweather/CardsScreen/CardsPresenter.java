package com.dakarkaret.distnweather.CardsScreen;

interface CardsPresenter {
    void updateAll();

    void deleteCity(int id, String cityName);

    void updateData();
}