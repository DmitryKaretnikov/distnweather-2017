package com.dakarkaret.distnweather.EditScreen;

import android.text.TextUtils;
import android.widget.Toast;

import com.dakarkaret.distnweather.Helper.ContextSingleton;
import com.dakarkaret.distnweather.Helper.PrefsHelper;
import com.dakarkaret.distnweather.Data.CityDbHelper;
import com.dakarkaret.distnweather.Data.CityObj;
import com.dakarkaret.distnweather.Data.CityObjSQLiteTypeMapping;
import com.dakarkaret.distnweather.Data.CityTable;
import com.pushtorefresh.storio.sqlite.impl.DefaultStorIOSQLite;
import com.pushtorefresh.storio.sqlite.queries.Query;

class EditPresenterImp implements EditPresenter {
    private DefaultStorIOSQLite mDefaultStorIOSQLite;
    private EditFragment mEditFragment;
    private int mCityId;

    private EditPresenterImp(int id, EditFragment editFragment) {
        mEditFragment = editFragment;
        mCityId = id;

        CityDbHelper sqLiteOpenHelper = new CityDbHelper();
        mDefaultStorIOSQLite = DefaultStorIOSQLite.builder()
                .sqliteOpenHelper(sqLiteOpenHelper)
                .addTypeMapping(CityObj.class, new CityObjSQLiteTypeMapping())
                .build();
    }

    static EditPresenterImp newInstance(int id, EditFragment editFragment) {
        return new EditPresenterImp(id, editFragment);
    }

    public void setId(int id) {
        mCityId = id;
    }

    public void loadCity() {
        CityObj cityObj = mDefaultStorIOSQLite.get()
                .object(CityObj.class)
                .withQuery(
                        Query.builder()
                                .table(CityTable.TABLE_NAME)
                                .where(CityTable._ID + " = ?")
                                .whereArgs(mCityId)
                                .build())
                .prepare()
                .executeAsBlocking();
        mEditFragment.setViews(cityObj);
    }

    public void deleteCity() {
        CityObj cityObj = new CityObj();
        cityObj.setId(mCityId);
        mDefaultStorIOSQLite
                .delete()
                .object(cityObj)
                .prepare()
                .executeAsBlocking();
        mEditFragment.startPrevious();
    }

    public void putCity(String name, String url, String latTxt, String lngTxt) {
        if (!TextUtils.isEmpty(latTxt) && !TextUtils.isEmpty(lngTxt) && !TextUtils.isEmpty(url) && !TextUtils.isEmpty(name)
                && !latTxt.equals(".") && !lngTxt.equals(".") && !latTxt.equals("-") && !lngTxt.equals("-")) {
            float lat = Float.valueOf(latTxt);
            float lng = Float.valueOf(lngTxt);

            if (lng <= 180 && lng >= -180 && lat <= 90 && lat >= -90) {
                CityObj cityObj = new CityObj();
                cityObj.setName(name);
                cityObj.setImageUrl(url);
                cityObj.setLat(lat);
                cityObj.setLng(lng);

                if (mCityId == -1) {
                    int id = PrefsHelper.getInstance().pullFreeId();
                    cityObj.setId(id);
                    putIO(cityObj);
                    mEditFragment.reloadEdit(id);
                } else {
                    cityObj.setId(mCityId);
                    putIO(cityObj);
                    mEditFragment.loadImage();
                }
            } else
                Toast.makeText(ContextSingleton.getContext(), "Something wrong with your location", Toast.LENGTH_SHORT).show();
        } else
            Toast.makeText(ContextSingleton.getContext(), "Please enter all fields with correct data", Toast.LENGTH_SHORT).show();
    }

    private void putIO(CityObj cityObj) {
        mDefaultStorIOSQLite
                .put()
                .object(cityObj)
                .prepare()
                .executeAsBlocking();
    }
}
