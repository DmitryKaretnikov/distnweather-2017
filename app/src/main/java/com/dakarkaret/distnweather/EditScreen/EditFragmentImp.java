package com.dakarkaret.distnweather.EditScreen;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.Toast;

import com.dakarkaret.distnweather.Activitiy.MapsActivity;
import com.dakarkaret.distnweather.Data.CityTable;
import com.dakarkaret.distnweather.Helper.ContextSingleton;
import com.dakarkaret.distnweather.Helper.PermHelper;
import com.dakarkaret.distnweather.Data.CityObj;
import com.dakarkaret.distnweather.R;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.OnFocusChange;
import butterknife.Unbinder;

public class EditFragmentImp extends Fragment implements EditFragment {
    private final static int MAP_REQUEST_CODE = 1, FILE_REQUEST_CODE = 2;
    @BindView(R.id.edit_image)
    ImageView mImageView;
    @BindView(R.id.edit_name_edit)
    EditText mNameEdit;
    @BindView(R.id.edit_lat_edit)
    EditText mLatEdit;
    @BindView(R.id.edit_lng_edit)
    EditText mLngEdit;
    @BindView(R.id.edit_url_edit)
    EditText mUrlEdit;
    @BindView(R.id.edit_choose_file_button)
    ImageButton mFileButton;
    private MenuItem menuItemDelete;
    private EditPresenter mEditPresenter;
    private Unbinder mUnbinder;
    private int mCityId;

    public static EditFragmentImp newInstance(int id) {
        Bundle bundle = new Bundle();
        bundle.putInt("CityId", id);
        EditFragmentImp fragment = new EditFragmentImp();
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
        setHasOptionsMenu(true);

        Bundle bundle = this.getArguments();
        if (bundle != null)
            mCityId = bundle.getInt("CityId", CityTable.NEW_CITY);
        else
            mCityId = CityTable.NEW_CITY;
        mEditPresenter = EditPresenterImp.newInstance(mCityId, this);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_edit, container, false);
        if (container != null) {
            container.clearDisappearingChildren();
        }
        mUnbinder = ButterKnife.bind(this, v);

        if (mCityId > 0) {
            disableHintAnim(v);
            mEditPresenter.loadCity();
            getActivity().setTitle("#" + mCityId);
        } else
            getActivity().setTitle("New city");

        return v;
    }

    private void disableHintAnim(View v) {
        TextInputLayout til = (TextInputLayout) v.findViewById(R.id.edit_name_input);
        til.setHintAnimationEnabled(false);
        til = (TextInputLayout) v.findViewById(R.id.edit_url_input);
        til.setHintAnimationEnabled(false);
        til = (TextInputLayout) v.findViewById(R.id.edit_lat_input);
        til.setHintAnimationEnabled(false);
        til = (TextInputLayout) v.findViewById(R.id.edit_lng_input);
        til.setHintAnimationEnabled(false);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater menuInflater) {
        super.onCreateOptionsMenu(menu, menuInflater);
        menuInflater.inflate(R.menu.menu_edit, menu);
        menuItemDelete = menu.findItem(R.id.menu_edit_item_delete);
        if (mCityId == CityTable.NEW_CITY) {
            menuItemDelete.setVisible(false);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_edit_item_save:
                showSaveDialog();
                return true;
            case R.id.menu_edit_item_delete:
                showDelDialog();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (data == null) {
            return;
        }
        if (requestCode == MAP_REQUEST_CODE) {
            mLatEdit.setText(String.valueOf(data.getDoubleExtra("lat", 0)));
            mLngEdit.setText(String.valueOf(data.getDoubleExtra("lng", 0)));
        }
        if (requestCode == FILE_REQUEST_CODE) {
            mUrlEdit.setText(data.getDataString());
            loadImage();
        }
    }

    @Override
    public void onDestroy() {
        mUnbinder.unbind();
        super.onDestroy();
    }

    @OnFocusChange(R.id.edit_url_edit)
    public void onFocusChange(boolean hasFocus) {
        if (!hasFocus) {
            loadImage();
        }
    }

    @OnClick(R.id.edit_setloc_button)
    public void setLocClick() {
        Intent intent = new Intent(getActivity(), MapsActivity.class);
        String latTxt = mLatEdit.getText().toString().trim();
        String lngTxt = mLngEdit.getText().toString().trim();
        if (!TextUtils.isEmpty(latTxt) && !TextUtils.isEmpty(lngTxt)
                && !latTxt.equals(".") && !lngTxt.equals(".") && !latTxt.equals("-") && !lngTxt.equals("-")) {
            double lat = Double.valueOf(latTxt);
            double lng = Double.valueOf(lngTxt);
            if (lng <= 180 && lng >= -180 && lat <= 90 && lat >= -90) {
                intent.putExtra("lat", lat);
                intent.putExtra("lng", lng);
            }
        }
        startActivityForResult(intent, MAP_REQUEST_CODE);
    }

    @OnClick(R.id.edit_choose_file_button)
    public void chooseFileClick() {
        if (PermHelper.checkFilePerm()) {
            Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
            intent.setType("file/*");
            startActivityForResult(intent, FILE_REQUEST_CODE);
        } else
            Toast.makeText(ContextSingleton.getContext(), "Please enable storage permission", Toast.LENGTH_SHORT).show();
    }

    private void showDelDialog() {
        new AlertDialog.Builder(getActivity())
                .setTitle("#" + mCityId)
                .setMessage("Are you sure to delete id: " + mCityId + "?")
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        mEditPresenter.deleteCity();
                    }
                })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                    }
                })
                .setCancelable(true)
                .show();
    }

    private void showSaveDialog() {
        String title;
        String message;
        if (mCityId != CityTable.NEW_CITY) {
            title = "#" + mCityId;
            message = "Are you sure to update id: " + mCityId + "?";
        } else {
            title = "New CityObj";
            message = "Are you sure to save new city?";
        }

        new AlertDialog.Builder(getActivity())
                .setTitle(title)
                .setMessage(message)
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        mEditPresenter.putCity(mNameEdit.getText().toString().trim(), mUrlEdit.getText().toString().trim(),
                                mLatEdit.getText().toString().trim(), mLngEdit.getText().toString().trim());
                    }
                })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                    }
                })
                .setCancelable(true)
                .show();
    }

    public void reloadEdit(int id) {
        mCityId = id;
        mEditPresenter.setId(id);
        getActivity().setTitle("#" + mCityId);
        menuItemDelete.setVisible(true);
    }

    public void setViews(CityObj city) {
        mNameEdit.setText(city.getName());
        mUrlEdit.setText(city.getImageUrl());
        mLatEdit.setText(String.valueOf(city.getLat()));
        mLngEdit.setText(String.valueOf(city.getLng()));
        loadImage();
    }

    public void loadImage() {
        String url = String.valueOf(mUrlEdit.getText());
        if (!TextUtils.equals(url, ""))
            Picasso.with(getActivity())
                    .load(url)
                    .placeholder(R.drawable.load_img)
                    .error(R.drawable.error_img)
                    .into(mImageView, new Callback() {
                        @Override
                        public void onSuccess() {
                        }

                        @Override
                        public void onError() {
                        }
                    });
    }

    public void startPrevious() {
        getActivity().onBackPressed();
    }
}