package com.dakarkaret.distnweather.EditScreen;

interface EditPresenter {
    void putCity(String name, String url, String latTxt, String lngTxt);

    void loadCity();

    void deleteCity();

    void setId(int id);
}
