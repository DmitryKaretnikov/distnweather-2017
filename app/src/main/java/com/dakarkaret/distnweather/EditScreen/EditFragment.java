package com.dakarkaret.distnweather.EditScreen;

import com.dakarkaret.distnweather.Data.CityObj;

interface EditFragment {
    void reloadEdit(int id);

    void setViews(CityObj city);

    void loadImage();

    void startPrevious();
}
