package com.dakarkaret.distnweather.Helper;

import android.content.Context;
import android.content.SharedPreferences;
import android.location.Location;

import java.text.DecimalFormat;

public class PrefsHelper {
    private static final String APP_PREFERENCES = "dnwSettings";
    private static final String LAST_LATITUDE = "latitude";
    private static final String LAST_LONGITUDE = "longitude";
    private static final String FREE_ID = "free_id";
    public static final float NO_PERM = 400;
    private static PrefsHelper mPrefsHelper;
    private SharedPreferences mSharedPreferences;

    private PrefsHelper() {
        mSharedPreferences = ContextSingleton.getContext().getSharedPreferences(APP_PREFERENCES, Context.MODE_PRIVATE);
    }

    public static synchronized PrefsHelper getInstance() {
        if (mPrefsHelper == null)
            mPrefsHelper = new PrefsHelper();
        return mPrefsHelper;
    }

    public void saveCoord(double lng, double lat) {
        SharedPreferences.Editor editor = mSharedPreferences.edit();
        editor.putFloat(LAST_LATITUDE, (float) lat);
        editor.putFloat(LAST_LONGITUDE, (float) lng);
        editor.apply();
    }

    public String getDistFrom(float lat, float lng) {
        if (mSharedPreferences.getFloat(LAST_LATITUDE, 1) != NO_PERM && PermHelper.checkLocPerm()) {
            Location locationA = new Location("point A");
            locationA.setLatitude(lat);
            locationA.setLongitude(lng);
            Location locationB = new Location("point B");
            locationB.setLatitude(mSharedPreferences.getFloat(LAST_LATITUDE, 1));
            locationB.setLongitude(mSharedPreferences.getFloat(LAST_LONGITUDE, 1));
            return "" + new DecimalFormat("##.###").format(locationA.distanceTo(locationB) / 1000) + " km";
        } else
            return "Please enable geolocation in app settings";
    }

    public int pullFreeId() {
        int id = mSharedPreferences.getInt(FREE_ID,4)+1;
        SharedPreferences.Editor editor = mSharedPreferences.edit();
        editor.putInt(FREE_ID, id);
        editor.apply();
        return id;
    }
}
