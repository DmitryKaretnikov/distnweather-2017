package com.dakarkaret.distnweather.Helper;

import com.dakarkaret.distnweather.Data.WeatherObj;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface OpenWeatherClientInterface {
    @GET("/data/2.5/weather")
    Call<WeatherObj> getWeatherObj(@Query("lat") String latitude,
                                   @Query("lon") String longitude,
                                   @Query("units") String units,
                                   @Query("appid") String appid);
}
