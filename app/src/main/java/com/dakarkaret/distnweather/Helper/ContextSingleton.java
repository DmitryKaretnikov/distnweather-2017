package com.dakarkaret.distnweather.Helper;

import android.content.Context;

public class ContextSingleton {
    private static ContextSingleton mContextSingleton;
    private static Context mContext;

    private ContextSingleton(Context context) {
        mContext = context.getApplicationContext();
    }

    public static Context getContext() {
        return mContext;
    }

    public static synchronized void setContextSingleton(Context context) {
        if (mContextSingleton == null)
            mContextSingleton = new ContextSingleton(context);
    }
}
