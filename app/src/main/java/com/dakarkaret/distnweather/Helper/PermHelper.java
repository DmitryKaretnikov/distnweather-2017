package com.dakarkaret.distnweather.Helper;

import android.Manifest;
import android.content.pm.PackageManager;
import android.support.v4.app.ActivityCompat;

public class PermHelper {
    public static boolean checkLocPerm() {
        return ActivityCompat.checkSelfPermission(ContextSingleton.getContext(), Manifest.permission.ACCESS_FINE_LOCATION)
                == PackageManager.PERMISSION_GRANTED;
    }

    public static boolean checkFilePerm() {
        return ActivityCompat.checkSelfPermission(ContextSingleton.getContext(), Manifest.permission.READ_EXTERNAL_STORAGE)
                == PackageManager.PERMISSION_GRANTED;
    }
}
