package com.dakarkaret.distnweather.Helper;

import im.delight.android.location.SimpleLocation;

public class LocationHelper {
    private static LocationHelper mLocationHelper;
    private SimpleLocation mLocation;

    private LocationHelper() {
        mLocation = new SimpleLocation(ContextSingleton.getContext());
        on();
    }

    public static synchronized LocationHelper getInstance() {
        if (mLocationHelper == null)
            mLocationHelper = new LocationHelper();
        return mLocationHelper;
    }

    public void on() {
        if (PermHelper.checkLocPerm())
            mLocation.beginUpdates();
    }

    public void off() {
        if (PermHelper.checkLocPerm())
            mLocation.endUpdates();
    }

    public double getLongitude() {
        return mLocation.getLongitude();
    }

    public double getLatitude() {
        return mLocation.getLatitude();
    }
}
